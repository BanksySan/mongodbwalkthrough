﻿namespace BanksySan.MongoDbWalkthrough
{
    using MongoDB.Bson;
    using MongoDB.Driver;
    using NUnit.Framework;

    [TestFixture]
    public class CrudTests
    {
        [Test]
        public void InsertDocument()
        {
            var client = new MongoClient();

            var database = client.GetDatabase("walkthrough");

            var collection = database.GetCollection<BsonDocument>("crud");

            var document = new BsonDocument { { "name", "alice" } };

            collection.InsertOne(document);

            Assert.That(document["_id"].IsObjectId);
        }

        [Test]
        public void GetAllDocuments()
        {
            var client = new MongoClient();

            var database = client.GetDatabase("walkthrough");
            var collection = database.GetCollection<BsonDocument>("crud");

            var filterDocument = new BsonDocument { { "name", "alice" } };

            var documents = collection.Find(filterDocument).ToList();

            foreach (var document in documents)
            {
                Assert.That(document["_id"].IsObjectId);
                Assert.That(document["name"].AsString, Is.EqualTo("alice"));
            }
        }

        [Test]
        public void ReplaceDocument()
        {
            var client = new MongoClient();

            var database = client.GetDatabase("walkthrough");
            var collection = database.GetCollection<BsonDocument>("crud");

            var aliceDocument = new BsonDocument { { "name", "alice" } };

            collection.InsertOne(aliceDocument);

            var filterDocument = new BsonDocument { { "name", "alice" } };

            var newDocument = new BsonDocument { { "name", "Alice" } };

            collection.ReplaceOne(filterDocument, newDocument);

            var newFilterDocument = new BsonDocument { { "_id", aliceDocument["_id"] } };

            var document = collection.Find(newFilterDocument).Single();

            Assert.That(document["name"].AsString, Is.EqualTo("Alice"));
        }

        [Test]
        public void RemoveDocument()
        {
            var client = new MongoClient();

            var database = client.GetDatabase("walkthrough");
            var collection = database.GetCollection<BsonDocument>("crud");

            var aliceDocument = new BsonDocument { { "name", "alice" } };

            collection.InsertOne(aliceDocument);

            var filterDocument = new BsonDocument { { "_id", aliceDocument["_id"] } };

            var result = collection.DeleteOne(filterDocument);

            Assert.That(result.DeletedCount, Is.EqualTo(1));
        }
    }
}